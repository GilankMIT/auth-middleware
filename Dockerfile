FROM alpine:latest

# Creating working directory
RUN mkdir /home/app
RUN apk add --no-cache ca-certificates
RUN apk add --no-cache tzdata

WORKDIR /home/app

COPY config/app.env /home/app/config/app.env
COPY database/ /home/app/database/

ADD auth-middleware /home/app/auth-middleware

EXPOSE 7000

ENTRYPOINT ["/home/app/auth-middleware"]
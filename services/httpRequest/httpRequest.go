package httpRequest

import (
	"bytes"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type HttpHeaders map[string]string

func PostData(url string, body []byte, headers HttpHeaders, timeoutInSec int) (code int, response []byte, err error) {
	clientHTTP := http.Client{}

	clientHTTP.Timeout = time.Second * 60 //set timeout to 1 minute (default)
	if timeoutInSec != 0 {
		clientHTTP.Timeout = time.Second * time.Duration(timeoutInSec) //set timeout to custom timeout
	}

	req, err := http.NewRequest("POST",
		url, bytes.NewReader(body))

	if err != nil {
		return 0, nil, err
	}

	//add header
	for key, val := range headers {
		req.Header.Set(key, val)
	}

	//execute http post
	resp, err := clientHTTP.Do(req)
	if err != nil {
		return 0, nil, nil
	}
	defer resp.Body.Close()

	//read response body
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return resp.StatusCode, nil, err
	}

	return resp.StatusCode, responseBody, nil
}

func PostDataWithBasicAuth(url string, username, password string, body []byte, headers HttpHeaders, timeoutInSec int) (code int, response []byte, err error) {
	//build basic auth header
	authToken := base64.URLEncoding.EncodeToString([]byte(username + ":" + password))

	if headers == nil {
		headers = make(map[string]string)
	}

	//pre-populate fixed header request
	headers["Authorization"] = "Basic " + authToken
	headers["Client-Id"] = username
	headers["Content-Type"] = "application/json"

	clientHTTP := http.Client{}

	clientHTTP.Timeout = time.Second * 60 //set timeout to 1 minute (default)
	if timeoutInSec != 0 {
		clientHTTP.Timeout = time.Second * time.Duration(timeoutInSec) //set timeout to custom timeout
	}

	req, err := http.NewRequest("POST",
		url, bytes.NewReader(body))

	if err != nil {
		return 0, nil, err
	}

	//add header
	for key, val := range headers {
		req.Header.Set(key, val)
	}

	//execute http post
	resp, err := clientHTTP.Do(req)
	if err != nil {
		return 0, nil, nil
	}
	defer resp.Body.Close()

	//read response body
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return resp.StatusCode, nil, err
	}

	return resp.StatusCode, responseBody, nil
}

func PostFormDataWithBasicAuth(url string, username, password string, formData url.Values, headers HttpHeaders, timeoutInSec int) (code int, response []byte, err error) {
	//build basic auth header
	authToken := base64.URLEncoding.EncodeToString([]byte(username + ":" + password))
	if headers == nil {
		headers = make(map[string]string)
	}

	headers["Authorization"] = "Basic " + authToken
	headers["Client-Id"] = username
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	clientHTTP := http.Client{}

	clientHTTP.Timeout = time.Second * 60 //set timeout to 1 minute (default)
	if timeoutInSec != 0 {
		clientHTTP.Timeout = time.Second * time.Duration(timeoutInSec) //set timeout to custom timeout
	}
	req, err := http.NewRequest("POST",
		url, strings.NewReader(formData.Encode()))

	if err != nil {
		return 0, nil, err
	}

	//add header
	for key, val := range headers {
		req.Header.Set(key, val)
	}

	//execute http post
	resp, err := clientHTTP.Do(req)
	if err != nil {
		return 0, nil, nil
	}
	defer resp.Body.Close()

	//read response body
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return resp.StatusCode, nil, err
	}

	return resp.StatusCode, responseBody, nil
}

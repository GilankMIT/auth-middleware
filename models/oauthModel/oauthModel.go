package oauthModel

import "github.com/jinzhu/gorm"

//mapping table
type (
	//OauthClients - mapping table oauth_clients
	OauthClients struct {
		gorm.Model
		ClientID       string `gorm:"column:client_id"`
		ClientSecret   string `gorm:"column:client_secret"`
		AliasClient    string `gorm:"column:alias_client"`
		Credential     string `gorm:"column:credential"`
		CredentialType string `gorm:"column:credential_type"`
		ClientDomain   string `gorm:"column:client_domain"`
		ClientScope    string `gorm:"column:client_scope"`
	}

	//OauthTokens - mapping table oauth_tokens
	OauthTokens struct {
		gorm.Model
		ClientID    string `gorm:"column:client_id"`
		AccessToken string `gorm:"column:access_token"`
		ExpiredAt   int64  `gorm:"column:expired_at"`
	}

	//VwOauthToken - mapping view vw_oauth_token
	VwOauthToken struct {
		ID           int64  `gorm:"column:id"`
		ClientID     string `gorm:"column:client_id"`
		AccessToken  string `gorm:"column:access_token"`
		ExpiredAt    int64  `gorm:"column:expired_at"`
		ClientSecret string `gorm:"column:client_secret"`
		ClientDomain string `gorm:"column:client_domain"`
		ClientScope  string `gorm:"column:client_scope"`
	}
)

//params for function
type (
	//DataOauthToken - object for params insert token
	DataOauthToken struct {
		ClientID    string
		AccessToken string
		ExpiredAt   int64
	}

	//LoginClientRequest - object for request login
	LoginClientRequest struct {
		Username  string `json:"username" binding:"required"`
		Password  string `json:"password" binding:"required"`
		GrantType string `json:"grant_type"`
		ClientId  string `json:"-"`
	}

	//LoginClientResponse - object for response login
	LoginClientResponse struct {
		AccessToken string `json:"access_token"`
		TokenType   string `json:"token_type"`
		ExpiresIn   int64  `json:"expires_in"`
		Scope       string `json:"scope"`
		Domain      string `json:"domain"`
	}

	//ClientData - object for Add Client Data (in Use Case)
	ClientData struct {
		ClientID       string `json:"client_id"`
		ClientSecret   string `json:"client_secret"`
		ClientDomain   string `json:"client_domain"`
		ClientAlias    string `json:"client_alias"`
		Credential     string `json:"credential"`
		CredentialType string `json:"credential_type"`
		ClientScope    string `json:"client_scope"`
	}

	//AddClientRequest - object for mapping add client request
	AddClientRequest struct {
		ClientID     string `json:"client_id" binding:"required" example:"EF-0012-3210-FF100"`
		ClientSecret string `json:"client_secret" binding:"required" example:"appsecret"`
		ClientDomain string `json:"client_domain" binding:"required" example:"nobueform.com"`
		ClientAlias  string `json:"client_alias" binding:"required" example:"admin@nobubank.com"`
		Credential   string `json:"credential" binding:"required" example:"thisisaplainpass"`
		ClientScope  string `json:"client_scope" binding:"required" example:"ef_admin"`
	}

	//AddClientResponse - object for mapping add client response
	AddClientResponse struct {
		ClientID       string `json:"client_id"`
		ClientSecret   string `json:"client_secret"`
		ClientDomain   string `json:"client_domain"`
		ClientAlias    string `json:"client_alias"`
		CredentialType string `json:"credential_type"`
		ClientScope    string `json:"client_scope"`
		Message        string `json:"message"`
	}

	//AuthVerificationRes - object for mapping successful auth verification process
	AuthVerificationRes struct {
		Message string `json:"message" example:"OK"`
	}
)

# Oauth Token

API Oauth Token

**URL** : `/api/v1/oauth/token`

**Method** : `POST`

**Auth required** : `Yes` `(Basic Auth)`

## Request 
**Header**<br>

**Content-Type** : `application/x-www-form-urlencoded`<br>

**Body**
 
```
"username": "51.79.144.226",
"password": "bb",
"grant_type": "bb"
```

## Success Response

**Response:**

**Header** : `200`
```json
{
    "messages": "success",
    "status": 200
}
```

**API Error:**

**Header** : `400`
```json
{
    "validation": "XXXXXXXXXX",
    "status": 400
}
```

**Header** : `401`
```json
{
    "messages": "Token is expired",
    "status": 401
}
```

**Header** : `403`
```json
{
    "messages": "XXXXXXXXXX",
    "status": 403
}
```

**Header** : `500`
```json
{
    "status": 500,
    "messages": "something went wrong"
}
```
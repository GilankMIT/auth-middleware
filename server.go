package main

import (
	"flag"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/GilankMIT/auth-middleware/database"
	"gitlab.com/GilankMIT/auth-middleware/routes/oauthHttp"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	configDb "bitbucket.org/AgieAja3108/nobu-connect-database/database/mysql"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/logger"
)

var rxURL = regexp.MustCompile(`^/regexp\d*`)

func main() {
	err := godotenv.Load("config/app.env")
	if err != nil {
		log.Error().Msg("Failed read configuration database")
		return
	}

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbName := os.Getenv("DB_NAME")
	maxIdle := os.Getenv("MAX_IDLE")
	maxConn := os.Getenv("MAX_CONN")
	dbDriver := os.Getenv("DB_DRIVER")

	myMaxIdle, errMaxIdle := strconv.Atoi(maxIdle)
	if errMaxIdle != nil {
		log.Error().Msg("Failed convert errMaxIdle = " + errMaxIdle.Error())
		return
	}

	myMaxConn, errMaxConn := strconv.Atoi(maxConn)
	if errMaxConn != nil {
		log.Error().Msg("Failed convert errMaxConn = " + errMaxConn.Error())
		return
	}

	conn, errConn := getDBConnection(dbHost, dbPort, dbUser, dbPass, dbName, myMaxIdle, myMaxConn)
	if errConn != nil {
		log.Error().Msg("Failed connect database errConn : " + errConn.Error())
		return
	}

	port := os.Getenv("PORT")
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if gin.IsDebugging() {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	log.Logger = log.Output(
		zerolog.ConsoleWriter{
			Out:     os.Stderr,
			NoColor: false,
		},
	)

	setMode := os.Getenv("SET_MODE")
	if setMode == "production" {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}

	r := gin.New()

	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"POST", "DELETE", "GET", "OPTIONS", "PUT"},
		AllowHeaders:     []string{"Origin", "Content-Type", "Authorization", "userid"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           1 * time.Minute,
	}))

	// Add a logger middleware, which:
	//   - Logs all requests, like a combined access and error log.
	//   - Logs to stdout.
	r.Use(logger.SetLogger())

	// Custom logger
	subLog := zerolog.New(os.Stdout).With().Logger()

	r.Use(logger.SetLogger(logger.Config{
		Logger:         &subLog,
		UTC:            true,
		SkipPath:       []string{"/skip"},
		SkipPathRegexp: rxURL,
	}))

	r.Use(gin.Recovery())

	if port == "" {
		log.Error().Msg("port cant empty")
		return
	}

	defer func() {
		errConnClose := conn.Close()
		if errConnClose != nil {
			log.Error().Msg("errConnClose : " + errConnClose.Error())
		}
	}()

	pathMigration := os.Getenv("PATH_MIGRATION")
	migrationDir := flag.String("migration-dir", pathMigration, "migration directory")
	log.Info().Msg("path migration : " + pathMigration)

	migrationConf, errMigrationConf := database.NewMigrationConfig(*migrationDir, dbHost, dbPort, dbUser, dbPass, dbName, dbDriver)
	if errMigrationConf != nil {
		log.Error().Msg(errMigrationConf.Error())
		return
	}

	errMigration := database.MigrateUp(migrationConf)
	if errMigration != nil {
		if errMigration.Error() != "no change" {
			log.Error().Msg(errMigration.Error())
			return
		}
		log.Info().Msg("Migration success : no change table . . .")
	}

	log.Info().Msg("Last Update : " + time.Now().Format("2006-01-02 15:04:05"))
	log.Info().Msg("Service Running version 0.0.1 at port : " + port)

	//modules oauth
	oauthHttp.OauthRoute(r, conn)

	//use ginSwagger middleware to
	//docs.SwaggerInfo.Schemes = []string{"http", "https"}
	//r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	if errHTTP := http.ListenAndServe(":"+port, r); errHTTP != nil {
		log.Error().Msg(errHTTP.Error())
	}
}

func getDBConnection(dbHost, dbPort, dbUser, dbPass, dbName string, maxIdle, maxConn int) (*gorm.DB, error) {
	conn, err := configDb.ConnMySQLORM(dbHost, dbPort, dbUser, dbPass, dbName, maxIdle, maxConn)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

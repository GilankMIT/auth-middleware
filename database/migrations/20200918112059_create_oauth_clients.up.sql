create table oauth_clients
(
    id bigint not null auto_increment primary key,
    client_id varchar(100) not null,
    client_secret varchar(255) not null,
    client_domain varchar(512) not null,
    alias_client varchar(100) not null,
    credential varchar(512),
    credential_type varchar(50),
    created_at datetime not null,
    updated_at datetime not null,
    deleted_at datetime
);
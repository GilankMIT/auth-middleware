create or replace view vw_oauth_tokens as
select ot.id,ot.client_id,ot.access_token,ot.expired_at,oc.client_domain, oc.client_scope, oc.client_secret
from oauth_tokens ot
         inner join oauth_clients oc on oc.client_id = ot.client_id
where ot.deleted_at is null and (UNIX_TIMESTAMP() * 1000) <= ot.expired_at;
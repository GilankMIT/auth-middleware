create table oauth_tokens
(
    id bigint not null auto_increment primary key,
    client_id varchar(100) not null,
    access_token varchar(512) not null,
    expired_at bigint not null,
    created_at datetime not null,
    updated_at datetime not null,
    deleted_at datetime
);
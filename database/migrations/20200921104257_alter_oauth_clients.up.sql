alter table oauth_clients
    add column client_scope varchar(50) not null default 'read' after credential_type;
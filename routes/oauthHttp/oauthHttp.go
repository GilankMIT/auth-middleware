package oauthHttp

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"

	middlewareUC "gitlab.com/GilankMIT/auth-middleware/modules/middlewares/middlewareUsecase"

	oauthHandler "gitlab.com/GilankMIT/auth-middleware/modules/oauths/oauthDelivery"
	oauthRepo "gitlab.com/GilankMIT/auth-middleware/modules/oauths/oauthRepository"
	oauthUC "gitlab.com/GilankMIT/auth-middleware/modules/oauths/oauthUsecase"
)

//OauthRoute - list route oauth
func OauthRoute(engine *gin.Engine, db *gorm.DB) {
	oauthRepository := oauthRepo.NewOauthRepository(db)

	middlewareUsecase := middlewareUC.NewMiddlewareUsecase(oauthRepository)
	oauthUsecase := oauthUC.NewOauthUsecase(oauthRepository)

	oauthHandler.NewOauthHTTPHandler(engine, oauthUsecase, middlewareUsecase)
}

# AUTH MIDDLEWARE #

This repository for authentication oauth 2.0 for internal service

### What is this repository for? ###

* get credential for connect to internal service

## API Documentation
## Features Oauth Token

| Service Name  | Description |
| ------------- |--------|
| [Login](docs/apis/oauth-token.md) | Get Oauth Token |

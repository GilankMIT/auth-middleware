package oauthDelivery

import (
	"bitbucket.org/AgieAja3108/nobu-helper-go/models/jsonModel"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/GilankMIT/auth-middleware/models/oauthModel"
	"gitlab.com/GilankMIT/auth-middleware/modules/middlewares"
	"gitlab.com/GilankMIT/auth-middleware/modules/oauths"
	"gitlab.com/GilankMIT/auth-middleware/modules/oauths/oauthUsecase"
	"gitlab.com/GilankMIT/auth-middleware/services/jsonHttpResponse"
	"gitlab.com/GilankMIT/auth-middleware/services/requestvalidationerror"
	"net/http"
)

type oauthHandler struct {
	oauthUsecase      oauths.OauthUsecase
	middlewareUsecase middlewares.MiddlewareUsecase
}

//NewOauthHTTPHandler - will create new an object implementation of oauths.OauthUsecase interface
func NewOauthHTTPHandler(r *gin.Engine, oauthUC oauths.OauthUsecase, middlewareUC middlewares.MiddlewareUsecase) {
	handler := oauthHandler{
		oauthUsecase:      oauthUC,
		middlewareUsecase: middlewareUC,
	}

	oauth := r.Group("/api/v1/oauth")
	oauth.Use(middlewareUC.BasicAuth)
	{
		oauth.POST("/token", handler.LoginClient)
	}

	oauthInternalAuth := r.Group("/api/v1/oauth")
	oauthInternalAuth.Use(middlewareUC.BasicAuthInternal)
	{
		oauthInternalAuth.POST("/create-client", handler.CreateClient)
	}

	oauthBearer := r.Group("/api/v1/oauth")
	oauthBearer.Use(middlewareUC.BearerAuth)
	{
		oauthBearer.POST("/verify-token", handler.VerifyToken)
	}
}

func (handler *oauthHandler) LoginClient(c *gin.Context) {
	clientId, exist := c.Get("clientId")
	if !exist {
		response := jsonModel.JSONResponseBadRequest{
			Status:     http.StatusBadRequest,
			Validation: "empty client id",
		}
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	username := c.PostForm("username")
	password := c.PostForm("password")
	grantType := c.PostForm("grant_type")
	if username == "" || password == "" || grantType == "" {
		response := jsonModel.JSONResponseBadRequest{
			Status:     http.StatusBadRequest,
			Validation: "username or password or grant type cant empty",
		}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	loginClientRequest := oauthModel.LoginClientRequest{
		Username:  username,
		Password:  password,
		GrantType: grantType,
		ClientId:  clientId.(string),
	}

	res, errRes := handler.oauthUsecase.LoginClient(&loginClientRequest)
	if errRes != nil {
		if errRes.Error() == "1" || errRes.Error() == "2" {
			response := jsonModel.JSONResponsePost{
				Status:   http.StatusForbidden,
				Messages: "Wrong username or password",
			}

			c.JSON(http.StatusForbidden, response)
			return
		}

		log.Error().Msg(errRes.Error())
		response := jsonModel.JSONErrorResponse{
			Status:   500,
			Messages: errRes.Error(),
			Target:   c.Request.RequestURI,
		}

		c.JSON(http.StatusInternalServerError, response)
		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"access_token": res.AccessToken,
			"token_type":   res.TokenType,
			"expires_in":   res.ExpiresIn,
			"scope":        res.Scope,
			"domain":       res.Domain,
		},
	)
	return
}

func (handler *oauthHandler) CreateClient(c *gin.Context) {
	var request oauthModel.AddClientRequest
	errBind := c.ShouldBind(&request)
	if errBind != nil {
		validations := requestvalidationerror.GetvalidationError(errBind)

		if len(validations) > 0 {
			jsonHttpResponse.NewFailedBadRequestResponse(c, validations)
			return
		}
		jsonHttpResponse.NewFailedBadRequestResponse(c, errBind.Error())
		return
	}

	//build dto
	addClientRequest := oauthModel.ClientData{
		ClientID:       request.ClientID,
		ClientSecret:   request.ClientSecret,
		ClientDomain:   request.ClientDomain,
		ClientAlias:    request.ClientAlias,
		Credential:     request.Credential,
		CredentialType: "password",
		ClientScope:    request.ClientScope,
	}

	savedClientData, err := handler.oauthUsecase.AddClient(addClientRequest)
	if err != nil {
		if err == oauthUsecase.ErrClientIDAlreadyExist {
			jsonHttpResponse.NewFailedBadRequestResponse(c, err.Error())
			return
		}
		jsonHttpResponse.NewFailedInternalServerResponse(c, err.Error())
		return
	}

	//build response object
	res := oauthModel.AddClientResponse{
		ClientID:       savedClientData.ClientID,
		ClientSecret:   savedClientData.ClientSecret,
		ClientDomain:   savedClientData.ClientDomain,
		ClientAlias:    savedClientData.AliasClient,
		CredentialType: savedClientData.CredentialType,
		ClientScope:    savedClientData.ClientScope,
		Message:        "client successfully added",
	}

	jsonHttpResponse.NewSuccessfulOKResponse(c, res)
	return
}

func (handler oauthHandler) VerifyToken(c *gin.Context) {
	//if request passed the middleware, it means that OAuth Token is valid
	//proceed to client's own authorization implementation
	res := oauthModel.AuthVerificationRes{Message: "ok"}
	jsonHttpResponse.NewSuccessfulOKResponse(c, res)
	return
}

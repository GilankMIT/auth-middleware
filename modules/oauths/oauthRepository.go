package oauths

import "gitlab.com/GilankMIT/auth-middleware/models/oauthModel"

//OauthRepository - repository interface for module oauth2.0
type OauthRepository interface {
	RetrieveUser(clientId, username, credType string) (*oauthModel.OauthClients, error)

	InsertOauthToken(clientID, accessToken string, expiredAt int64) error
	RetrieveOauthToken(clientID string) (*oauthModel.VwOauthToken, error)

	RetrieveClientSecret(clientID string) (*string, error)

	//InsertOauthClient insert oauthModel.OauthClients data to repository
	InsertOauthClient(oauthClientModel oauthModel.OauthClients) (*oauthModel.OauthClients, error)
}

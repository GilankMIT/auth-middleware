package oauths

import "gitlab.com/GilankMIT/auth-middleware/models/oauthModel"

//OauthUsecase - usecase interface for module oauth2.0
type OauthUsecase interface {
	//LoginClient validate user credential and return OAuth Token
	LoginClient(request *oauthModel.LoginClientRequest) (*oauthModel.LoginClientResponse, error)
	//AddClient store client data and credential to database for later authentication and OAuth Token generation
	AddClient(clientData oauthModel.ClientData) (*oauthModel.OauthClients, error)
}

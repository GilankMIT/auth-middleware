package oauthUsecase

import (
	helperJwt "bitbucket.org/AgieAja3108/nobu-helper-go/jwt"
	"bitbucket.org/AgieAja3108/nobu-helper-go/models/jsonModel"
	helperValidation "bitbucket.org/AgieAja3108/nobu-helper-go/validation"
	"errors"
	"gitlab.com/GilankMIT/auth-middleware/models/oauthModel"
	"gitlab.com/GilankMIT/auth-middleware/modules/oauths"
	"gitlab.com/GilankMIT/auth-middleware/services/bcrypthelper"
	"os"
	"strconv"
	"time"
)

var (
	ErrClientIDAlreadyExist = errors.New("client id already exist")
	ErrInvalidCredential    = errors.New("invalid credential")
)

type oauthUsecase struct {
	oauthRepository oauths.OauthRepository
}

//NewOauthUsecase - will create new an oauthUsecase object representation of oauths.OauthUsecase interface
func NewOauthUsecase(oauthRepo oauths.OauthRepository) oauths.OauthUsecase {
	return &oauthUsecase{
		oauthRepository: oauthRepo,
	}
}

//AddClient add client to repo
func (o *oauthUsecase) AddClient(clientData oauthModel.ClientData) (*oauthModel.OauthClients, error) {
	//data integrity check to make sure no clientId already exists
	_, err := o.oauthRepository.RetrieveClientSecret(clientData.ClientID)
	if err == nil {
		return nil, ErrClientIDAlreadyExist
	}

	//create model
	//convert client credential to BCrypt
	hashedClientCredential, err := bcrypthelper.GenerateBcryptCustomCost(clientData.Credential, 14)
	if err != nil {
		return nil, err
	}

	clientModel := oauthModel.OauthClients{
		ClientID:       clientData.ClientID,
		ClientSecret:   clientData.ClientSecret,
		AliasClient:    clientData.ClientAlias,
		Credential:     hashedClientCredential,
		CredentialType: clientData.CredentialType,
		ClientDomain:   clientData.ClientDomain,
		ClientScope:    clientData.ClientScope,
	}

	newClientData, err := o.oauthRepository.InsertOauthClient(clientModel)
	if err != nil {
		return nil, err
	}

	return newClientData, err
}

//LoginClient - use case for get token bearer
func (oauthUC *oauthUsecase) LoginClient(request *oauthModel.LoginClientRequest) (*oauthModel.LoginClientResponse, error) {
	//get data oauth client
	oauthClient, err := oauthUC.oauthRepository.RetrieveUser(request.ClientId, request.Username, request.GrantType)
	if err != nil {
		return nil, err
	}

	//compare hash password
	checkPassword := helperValidation.CheckPasswordHash(request.Password, oauthClient.Credential)
	if checkPassword == false {
		//wrong password
		return nil, errors.New("2")
	}

	expDuration, errConvert := strconv.Atoi(os.Getenv("EXP_DURATION"))
	if errConvert != nil {
		return nil, errors.New("oauthUC.LoginClient errConvert :" + errConvert.Error())
	}

	loginExpDuration := time.Duration(expDuration) * time.Minute
	//generate token jwt
	myToken, errToken := helperJwt.GenerateClientTokenBearer(oauthClient.ClientID, oauthClient.ClientSecret, oauthClient.ClientDomain, loginExpDuration)
	if errToken != nil {
		return nil, errToken
	}

	expAt := time.Now().Add(loginExpDuration).Unix() * 1000

	errInsert := oauthUC.oauthRepository.InsertOauthToken(oauthClient.ClientID, myToken, expAt)
	if errInsert != nil {
		return nil, errInsert
	}

	loginResponse := oauthModel.LoginClientResponse{
		AccessToken: myToken,
		TokenType:   jsonModel.BEARER,
		ExpiresIn:   expAt,
		Scope:       oauthClient.ClientScope,
		Domain:      oauthClient.ClientDomain,
	}

	return &loginResponse, nil
}

package oauths

import (
	"bitbucket.org/AgieAja3108/nobu-helper-go/jwt"
	"gitlab.com/GilankMIT/auth-middleware/models/oauthModel"
)

const (
	OauthGrantTypePassword = "password"
)

//OauthService is an interface used for 3rd party application
type OauthService interface {
	//AddClient store client data and credential to database for later authentication and OAuth Token generation
	AddClient(clientData oauthModel.AddClientRequest) (*oauthModel.OauthClients, error)
	//GetToken used to retrieve client token
	GetToken(request oauthModel.LoginClientRequest, clientId, secret string) (*oauthModel.LoginClientResponse, error)
	//VerifyBearerAuth used to verify auth token
	VerifyBearerAuth(clientId, bearer string) (isVerified bool, err error)
	//GetClaims used to retrieve JWT Claims
	GetClaims(token string) (claims *jwt.MyClientClaims, err error)
}

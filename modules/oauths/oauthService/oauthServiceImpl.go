package oauthService

import (
	"bitbucket.org/AgieAja3108/nobu-helper-go/jwt"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/GilankMIT/auth-middleware/models/oauthModel"
	"gitlab.com/GilankMIT/auth-middleware/modules/oauths"
	"gitlab.com/GilankMIT/auth-middleware/services/httpRequest"
	"net/url"
	"strings"
)

var (
	ErrClientIDAlreadyExist = errors.New("client id already exist")
	ErrInvalidCredential    = errors.New("invalid credential")
	ErrUnknownUser          = errors.New("unknown user. (have you specified client id?)")
	ErrUnreachableServer    = errors.New("unreachable auth server. Check config or make sure auth server is running")
)

const (
	GetTokenURI    = "/api/v1/oauth/token"
	AddClientURI   = "/api/v1/oauth/create-client"
	VerifyTokenURI = "/api/v1/oauth/verify-token"
)

type oauthService struct {
	oauthRepo oauths.OauthRepository

	oauthURL               string
	oauthBasicAuthUsername string
	oauthBasicAuthPassword string
}

//NewOauthService implement oauths.OauthService
func NewOauthService(oauthURL, oauthBasicAuthUsername, oauthBasicAuthPassword string) oauths.OauthService {
	//implement interface and
	//inject repo with Oauth server url config
	return &oauthService{oauthURL: oauthURL,
		oauthBasicAuthPassword: oauthBasicAuthPassword,
		oauthBasicAuthUsername: oauthBasicAuthUsername}
}

//AddClient add client to repo
func (o oauthService) AddClient(clientData oauthModel.AddClientRequest) (*oauthModel.OauthClients, error) {
	//build http body
	request := oauthModel.AddClientRequest{
		ClientID:     clientData.ClientID,
		ClientSecret: clientData.ClientSecret,
		ClientDomain: clientData.ClientDomain,
		ClientAlias:  clientData.ClientAlias,
		Credential:   clientData.Credential,
		ClientScope:  clientData.ClientScope,
	}
	reqBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	httpCode, respBody, err := httpRequest.PostDataWithBasicAuth(
		o.oauthURL+AddClientURI,
		o.oauthBasicAuthUsername,
		o.oauthBasicAuthPassword,
		reqBody,
		nil,
		120)
	if err != nil {
		return nil, err
	}
	//check http code
	if httpCode >= 400 &&
		httpCode < 405 {
		//build error message
		var errBody struct {
			Status  int    `json:"status"`
			Message string `json:"message"`
		}

		errBadResp := json.Unmarshal(respBody, &errBody)
		if errBadResp != nil {
			return nil, errors.New("problem in auth middleware")
		}

		if errBody.Message == ErrClientIDAlreadyExist.Error() {
			return nil, ErrClientIDAlreadyExist
		}

		return nil, ErrInvalidCredential
	} else if httpCode > 405 {
		return nil, errors.New("err addClient : " + fmt.Sprint(httpCode))
	}

	//build respBody
	var res oauthModel.AddClientResponse
	err = json.Unmarshal(respBody, &res)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

//GetToken authenticate user and retrieve token
func (o oauthService) GetToken(request oauthModel.LoginClientRequest, clientId, secret string) (*oauthModel.LoginClientResponse, error) {
	//build http body
	reqBody := url.Values{}
	reqBody.Add("username", request.Username)
	reqBody.Add("password", request.Password)
	reqBody.Add("grant_type", request.GrantType)

	httpCode, respBody, err := httpRequest.PostFormDataWithBasicAuth(
		o.oauthURL+GetTokenURI,
		clientId, secret, reqBody, nil, 120)
	if err != nil {
		return nil, err
	}

	//check if http code is 0 (means server unreachable)
	if httpCode == 0 {
		return nil, ErrUnreachableServer
	}

	//check http code
	if httpCode > 400 &&
		httpCode < 405 {
		return nil, ErrInvalidCredential
	} else if httpCode > 405 {
		return nil, errors.New("err login : " + fmt.Sprint(httpCode))
	}

	//build respBody
	var res oauthModel.LoginClientResponse
	err = json.Unmarshal(respBody, &res)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

//VerifyBearerAuth verify authentication token given by
func (o oauthService) VerifyBearerAuth(clientId, bearer string) (isVerified bool, err error) {
	if clientId == "" {
		return false, ErrUnknownUser
	}

	headers := map[string]string{
		"Client-Id":     clientId,
		"Authorization": bearer,
	}

	httpCode, _, err := httpRequest.PostData(
		o.oauthURL+VerifyTokenURI,
		nil,
		headers, 120)

	if err != nil {
		return false, err
	}

	if httpCode > 210 {
		return false, ErrInvalidCredential
	}

	return true, nil
}

//GetClaims return claims of token
func (o oauthService) GetClaims(token string) (claims *jwt.MyClientClaims, err error) {
	//split token
	bearerArr := strings.Split(token, " ")
	if len(bearerArr) < 2 {
		return nil, errors.New("token is not a valid jwt")
	}

	tokenPart := strings.Split(bearerArr[1], ".")
	if len(tokenPart) < 3 {
		return nil, errors.New("token is not a valid jwt")
	}

	//parse to string from base64
	parsedClaimToken, err := base64.RawURLEncoding.DecodeString(tokenPart[1])
	if err != nil {
		return nil, err
	}
	var jwtClaim jwt.MyClientClaims
	err = json.Unmarshal(parsedClaimToken, &jwtClaim)
	if err != nil {
		return nil, err
	}
	return &jwtClaim, nil
}

package oauthRepository

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/GilankMIT/auth-middleware/models/oauthModel"
	"gitlab.com/GilankMIT/auth-middleware/modules/oauths"
)

type sqlRepository struct {
	Conn *gorm.DB
}

//NewOauthRepository - will create an object that representation that oauths.OauthRepository
func NewOauthRepository(Conn *gorm.DB) oauths.OauthRepository {
	return &sqlRepository{Conn}
}

//RetrieveUser - get data user
//@param username : alias_client
func (db *sqlRepository) RetrieveUser(clientId, username, credType string) (*oauthModel.OauthClients, error) {
	var oauthClient oauthModel.OauthClients
	err := db.Conn.Select("client_id,client_secret,client_domain,credential,client_scope").
		Where("client_id = ? AND alias_client = ? and credential_type = ?", clientId,
			username, credType).First(&oauthClient).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.New("1")
		}

		return nil, errors.New("oauthRepo.RetrieveUser err : " + err.Error())
	}

	return &oauthClient, nil
}

//InsertOauthClient - insert data to oauth_client
func (db *sqlRepository) InsertOauthClient(oauthClientModel oauthModel.OauthClients) (*oauthModel.OauthClients, error) {
	dbConn := db.Conn.Create(&oauthClientModel)
	if dbConn.Error != nil {
		return nil, dbConn.Error
	}

	return &oauthClientModel, nil
}

//InsertOauthToken - insert table oauth_tokens
func (db *sqlRepository) InsertOauthToken(clientID, accessToken string, expiredAt int64) error {
	tx := db.Conn.Begin()

	if tx.Error != nil {
		return errors.New("oauthRepo.InsertOauthToken : " + tx.Error.Error())
	}

	oauthToken := oauthModel.OauthTokens{
		ClientID:    clientID,
		AccessToken: accessToken,
		ExpiredAt:   expiredAt,
	}

	errDelete := tx.Where("client_id = ?", clientID).Delete(&oauthToken).Error
	if errDelete != nil {
		tx.Rollback()
		return errors.New("oauthRepo.InsertOauthToken errDelete : " + errDelete.Error())
	}

	err := tx.Create(&oauthToken).Error
	if err != nil {
		tx.Rollback()
		return errors.New("oauthRepo.InsertOauthToken err : " + err.Error())
	}

	tx.Commit()
	return nil
}

//RetrieveOauthToken -get client secret,client domain from vw_oauth_tokens
func (db *sqlRepository) RetrieveOauthToken(clientID string) (*oauthModel.VwOauthToken, error) {
	var oauthToken oauthModel.VwOauthToken
	err := db.Conn.Select("client_secret,client_domain").Where("client_id = ?", clientID).First(&oauthToken).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.New("1")
		}

		return nil, errors.New("oauthRepo.RetrieveOauthToken err : " + err.Error())
	}

	return &oauthToken, nil
}

//RetrieveClientSecret - get client_secret from table oauth_clients
func (db *sqlRepository) RetrieveClientSecret(clientID string) (*string, error) {
	var oauthClient oauthModel.OauthClients
	err := db.Conn.Select("client_secret").Where("client_id = ?", clientID).First(&oauthClient).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.New("1")
		}

		return nil, errors.New("oauthRepo.RetrieveClientSecret err : " + err.Error())
	}

	clientSecret := oauthClient.ClientSecret
	return &clientSecret, nil
}

package middlewareUsecase

import (
	"bitbucket.org/AgieAja3108/nobu-helper-go/models/jsonModel"
	"encoding/base64"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/GilankMIT/auth-middleware/modules/middlewares"
	"gitlab.com/GilankMIT/auth-middleware/modules/oauths"
	"net/http"
	"strings"
)

type middlewareUsecase struct {
	oauthRepository oauths.OauthRepository
}

//NewMiddlewareUsecase - will create new an oauthUsecase object representation of middlewares.MiddlewareUsecase interface
func NewMiddlewareUsecase(oauthRepo oauths.OauthRepository) middlewares.MiddlewareUsecase {
	return &middlewareUsecase{
		oauthRepository: oauthRepo,
	}
}

//BasicAuth - function for validation basic auth
func (middlewareUC *middlewareUsecase) BasicAuth(c *gin.Context) {
	username := c.PostForm("username")
	//password := c.PostForm("password")
	grantType := c.PostForm("grant_type")

	//retrieve client id by basic token
	basicAuthHeader := c.GetHeader("Authorization")
	basicAuthArr := strings.Split(basicAuthHeader, " ")
	if len(basicAuthArr) < 2 {
		response := jsonModel.JSONResponsePost{
			Status:   http.StatusForbidden,
			Messages: "invalid basic auth",
		}
		c.AbortWithStatusJSON(http.StatusForbidden, response)
		return
	}

	basicAuthToken := basicAuthArr[1]
	basicAuthPlain, errDecode := base64.URLEncoding.DecodeString(basicAuthToken)
	if errDecode != nil {
		response := jsonModel.JSONResponsePost{
			Status:   http.StatusForbidden,
			Messages: "invalid basic auth " + errDecode.Error(),
		}
		c.AbortWithStatusJSON(http.StatusForbidden, response)
		return
	}

	basicAuthPlainArr := strings.Split(string(basicAuthPlain), ":")
	if len(basicAuthPlainArr) < 2 {
		response := jsonModel.JSONResponsePost{
			Status:   http.StatusForbidden,
			Messages: "invalid basic auth",
		}
		c.AbortWithStatusJSON(http.StatusForbidden, response)
		return
	}

	clientId := basicAuthPlainArr[0]

	oauthClient, err := middlewareUC.oauthRepository.RetrieveUser(clientId, username, grantType)
	if err != nil {
		if err.Error() == "1" {
			response := jsonModel.JSONResponsePost{
				Status:   http.StatusForbidden,
				Messages: "Wrong username or password",
			}

			c.AbortWithStatusJSON(http.StatusForbidden, response)
			return
		}

		response := jsonModel.JSONErrorResponse{
			Status:   http.StatusInternalServerError,
			Messages: err.Error(),
			Target:   c.Request.RequestURI,
		}
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}

	authHeader := c.Request.Header.Get("Authorization")
	if !strings.Contains(authHeader, "Basic") {
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusUnauthorized,
			Messages: "Invalid Token",
			Target:   c.Request.RequestURI,
		}
		c.AbortWithStatusJSON(http.StatusUnauthorized, result)
		return
	}

	clientID := oauthClient.ClientID
	clientSecret := oauthClient.ClientSecret

	tokenString := strings.Replace(authHeader, "Basic ", "", -1)
	myToken := clientID + ":" + clientSecret
	myBasicAuth := base64.StdEncoding.EncodeToString([]byte(myToken))
	if tokenString != myBasicAuth {
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusUnauthorized,
			Messages: "Unauthorized user",
			Target:   c.Request.RequestURI,
		}
		c.AbortWithStatusJSON(http.StatusUnauthorized, result)
		return
	}

	c.Set("clientId", clientId)
	c.Next()
	return
}

//BearerAuth - function for validation bearer auth
func (middlewareUC *middlewareUsecase) BearerAuth(c *gin.Context) {
	myClientID := c.Request.Header.Get("Client-Id")
	target := c.Request.RequestURI
	host := c.Request.Host
	log.Info().Msg("Host : " + host)
	if myClientID == "" {
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusForbidden,
			Messages: "Unknown User",
			Target:   target,
		}
		c.AbortWithStatusJSON(http.StatusForbidden, result)
		return
	}

	//get domain,expired_at
	oauthToken, errClientSecret := middlewareUC.oauthRepository.RetrieveOauthToken(myClientID)
	if errClientSecret != nil {
		if errClientSecret.Error() == "1" {
			result := jsonModel.JSONErrorResponse{
				Status:   http.StatusUnauthorized,
				Messages: "Unauthorized user",
				Target:   target,
			}
			c.AbortWithStatusJSON(http.StatusUnauthorized, result)
			return
		}
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusInternalServerError,
			Messages: errClientSecret.Error(),
			Target:   target,
		}
		c.AbortWithStatusJSON(http.StatusInternalServerError, result)
		return
	}

	//if oauthToken.ClientDomain != host {
	//	result := jsonModel.JSONErrorResponse{
	//		Status:   http.StatusForbidden,
	//		Messages: "You not allowed for this url",
	//		Target:   target,
	//	}
	//	c.AbortWithStatusJSON(http.StatusForbidden, result)
	//	return
	//}

	authHeader := c.Request.Header.Get("Authorization")
	if !strings.Contains(authHeader, "Bearer") {
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusForbidden,
			Messages: "Invalid Token",
			Target:   target,
		}
		c.AbortWithStatusJSON(http.StatusForbidden, result)
		return
	}

	clientSecret := oauthToken.ClientSecret
	jwtSigningMethod := jwt.SigningMethodHS256
	jwtSignatureKey := []byte(clientSecret)
	tokenString := strings.Replace(authHeader, "Bearer ", "", -1)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("sinvalid signing method")
		} else if method != jwtSigningMethod {
			return nil, fmt.Errorf("signing method invalid")
		}

		return jwtSignatureKey, nil
	})

	if err != nil {
		log.Error().Msg(err.Error())
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusUnauthorized,
			Messages: err.Error(),
			Target:   target,
		}
		c.AbortWithStatusJSON(http.StatusUnauthorized, result)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok {
		clientID := claims["client_id"].(string)
		if myClientID != clientID {
			result := jsonModel.JSONErrorResponse{
				Status:   http.StatusUnauthorized,
				Messages: "Unauthorized user",
				Target:   target,
			}
			c.AbortWithStatusJSON(http.StatusUnauthorized, result)
			return
		}
	}

	c.Next()
}

//BasicAuthInternal - function for validation basic auth internal
func (middlewareUC *middlewareUsecase) BasicAuthInternal(c *gin.Context) {
	myClientID := c.Request.Header.Get("Client-Id")
	if myClientID == "" {
		response := jsonModel.JSONResponsePost{
			Status:   http.StatusForbidden,
			Messages: "You dont have access",
		}

		c.AbortWithStatusJSON(http.StatusForbidden, response)
		return
	}

	myClientSecret, err := middlewareUC.oauthRepository.RetrieveClientSecret(myClientID)
	if err != nil {
		if err.Error() == "1" {
			response := jsonModel.JSONResponsePost{
				Status:   http.StatusForbidden,
				Messages: "You dont have access",
			}

			c.AbortWithStatusJSON(http.StatusForbidden, response)
			return
		}

		response := jsonModel.JSONErrorResponse{
			Status:   http.StatusInternalServerError,
			Messages: err.Error(),
			Target:   c.Request.RequestURI,
		}
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}

	authHeader := c.Request.Header.Get("Authorization")
	if !strings.Contains(authHeader, "Basic") {
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusUnauthorized,
			Messages: "Invalid Token",
			Target:   c.Request.RequestURI,
		}
		c.AbortWithStatusJSON(http.StatusUnauthorized, result)
		return
	}

	clientID := myClientID
	clientSecret := *myClientSecret

	tokenString := strings.Replace(authHeader, "Basic ", "", -1)
	myToken := clientID + ":" + clientSecret
	myBasicAuth := base64.StdEncoding.EncodeToString([]byte(myToken))
	if tokenString != myBasicAuth {
		result := jsonModel.JSONErrorResponse{
			Status:   http.StatusUnauthorized,
			Messages: "Unauthorized user",
			Target:   c.Request.RequestURI,
		}
		c.AbortWithStatusJSON(http.StatusUnauthorized, result)
		return
	}

	c.Next()
	return
}

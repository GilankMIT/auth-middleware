package middlewares

import "github.com/gin-gonic/gin"

type MiddlewareUsecase interface {
	BasicAuth(c *gin.Context)
	BearerAuth(c *gin.Context)
	BasicAuthInternal(c *gin.Context)
}
